
const map = L.map('map').setView([48.2, 16.3], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; OpenStreetMap'
}).addTo(map);

const markers = L.markerClusterGroup();

navigator.geolocation.getCurrentPosition(
    (position) => {
        const userLat = position.coords.latitude;
        const userLng = position.coords.longitude;
        map.setView([userLat, userLng], 13);
        map.addLayer(markers);


        async function fetchData() {
            const userLatLng = L.latLng(userLat, userLng);
            markers.clearLayers();

            try {

                const res = await fetch('http://wifi.1av.at/trinken.php')
                const data = await res.json()
                data.features.forEach(feature => {
                    const coordinates = feature.geometry.coordinates.reverse();
                    const trinkbrunnenLatLng = L.latLng(coordinates[0], coordinates[1]);
                    const distance = userLatLng.distanceTo(trinkbrunnenLatLng);
                    const basisTypTxt = feature.properties.BASIS_TYP_TXT;
                    if (distance <= 1000) {
                        const marker = L.marker(coordinates);
                        markers.addLayer(marker);
                        marker.bindPopup(basisTypTxt);
                    }
                });

            } catch (error) {
                console.error('Error getting features:', error);
            }

        }


        fetchData();


        map.on('moveend', fetchData);
    },
    (error) => {
        console.error('Error getting user geolocation:', error);
    }
);

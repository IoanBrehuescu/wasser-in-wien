
        const map = L.map('map').setView([48.2, 16.3], 13);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; OpenStreetMap'
        }).addTo(map);

        const markers = L.markerClusterGroup(); 
        map.addLayer(markers);

       
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const userLat = position.coords.latitude;
                const userLng = position.coords.longitude;
                map.setView([userLat, userLng], 13);

           
                map.on('moveend', function () {
                    const bounds = map.getBounds();
                    const sw = bounds.getSouthWest();
                    const ne = bounds.getNorthEast();
                    const url = `http://wifi.1av.at/trinken.php?swLat=${sw.lat}&swLng=${sw.lng}&neLat=${ne.lat}&neLng=${ne.lng}`;

                    fetch(url)
                        .then(response => response.json())
                        .then(data => {
                            markers.clearLayers(); 
                            data.features.forEach(feature => {
                                const coordinates = feature.geometry.coordinates.reverse();
                                const marker = L.marker(coordinates);
                                markers.addLayer(marker);
                            });
                        })
                        .catch(error => {
                            console.error('Error fetching Trinkbrunnen data:', error);
                        });
                });
            },
            (error) => {
                console.error('Error getting user geolocation:', error);
            }
        );
    